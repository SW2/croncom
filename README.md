# Croncom

- [Description](#description)
- [Usage](#usage)
- [Annotations](#annotations)
- [Credits](#credits)
- [License](#license)

## Description

Simple tool which helps with maintenance of cron tasks. Croncom = cron commands.

**WARNING:** This is not stable release! Do not use it on production!

It requires **PHP >= 5.5**, Nette Framework and Kdyby Console.

## Usage

1) Add extension `Sw2\Croncom\DI\CroncomExtension`, for more information about configuration see the class definition.

```neon
extension:
    croncom: Sw2\Croncom\DI\CroncomExtension
```

2) Define your tasks:

```php
/**
 * @cron  *\5  *  *  *  *
 */
class MyCoolTask extends Sw2\Croncom\Task
{
	// here i can use injects and black magic
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		// do some awesome work
	}
	
}
```

3) Add it to runner:

```neon
croncom:
    tasks:
        - Namespace\To\MyCoolTask
```

4) Set crontab on your server for `* * * * *` for command `php www/index.php app:cron`

5) Enjoy!


## Annotations

### `@cron`

This anotation is required for automatic running. Define it as you know it from linux.

If you want to define e.g. `*/5` (every 5 minutes), use backslash e.g. `*\5`. Try it... you will know why.

## Credits

Dependent on [Kdyby/Console](https://github.com/Kdyby/Console) - this extension is here to provide integration of Symfony Console into Nette Framework.

Inspired by [stekycz/Cronner](https://github.com/stekycz/Cronner) - great idea, look at it if you want to solve cron tasks on production. His project is more mature.

Look also on [mtdowling/cron-expression](https://packagist.org/packages/mtdowling/cron-expression) - it is used on annotation processing. See for more information about definition of cron task.

## License

BSD
