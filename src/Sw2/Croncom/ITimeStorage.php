<?php

namespace Sw2\Croncom;

use Nette\Utils\DateTime;

/**
 * Interface ITimeStorage
 *
 * @package Sw2\Croncom
 */
interface ITimeStorage
{

	/**
	 * @param string $taskName
	 *
	 * @return DateTime
	 */
	public function getLastTime($taskName);

	/**
	 * @param string $taskName
	 * @param DateTime $time
	 */
	public function putLastTime($taskName, DateTime $time);

}
