<?php

namespace Sw2\Croncom;

use Cron\CronExpression;
use Nette\Reflection\ClassType;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CronRunner
 *
 * @package Sw2\Croncom
 */
final class CronRunner extends Command
{
	/** @var ITimeStorage */
	private $timeStorage;

	/** @var Task[] */
	private $tasks = [];

	/**
	 * @inheritdoc
	 */
	public function __construct(ITimeStorage $timeStorage)
	{
		parent::__construct('app:cron');
		$this->setDescription('Run cron tasks');
		$this->timeStorage = $timeStorage;
	}

	/**
	 * @param Task $task
	 */
	public function addTask(Task $task)
	{
		$this->tasks[] = $task;
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return int|null|void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$execTime = new DateTime;
		$output->writeln(sprintf('<info>Cron started at %s</info>', $execTime->format('H:i:s')));

		foreach ($this->tasks as $task) {
			try {
				if ($this->shouldStart($execTime, $task)) {
					$statusCode = $task->run($input, $output, $execTime);
					if ($statusCode === 0) {
						$this->timeStorage->putLastTime($task->getName(), $execTime);
					}
					else {
						$output->writeln(sprintf('<error>Status code %d</error> in task %s', $statusCode, $task->getName()));

						return $statusCode;
					}
				}
			}
			catch (\Exception $e) {
				$output->writeln(sprintf('<error>Error in task %s</error> - %s', $task->getName(), $e->getMessage()));
			}
		}

		$finishTime = new DateTime;
		$output->writeln(sprintf('<info>Cron finished at %s</info>', $finishTime->format('H:i:s')));
	}

	/**
	 * @param DateTime $time
	 * @param Task $task
	 *
	 * @return bool
	 */
	private function shouldStart(DateTime $time, Task $task)
	{
		$execTime = $time->modifyClone('+ 4 seconds');
		$lastTime = $this->timeStorage->getLastTime($task->getName());
		if ($lastTime === NULL) return TRUE;

		$annotations = ClassType::from($task)->getAnnotations();
		foreach ($annotations['cron'] as $expression) {
			$expression = CronExpression::factory(Strings::replace($expression, '~\\\\~', '/'));
			$next = $expression->getNextRunDate($lastTime)->getTimestamp();

			if ($execTime->getTimestamp() > $next) {
				return TRUE;
			}
		}

		return FALSE;
	}

}
