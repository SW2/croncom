<?php

namespace Sw2\Croncom\DI;

use Kdyby\Console\DI\ConsoleExtension;
use Nette;
use Nette\DI\CompilerExtension;
use Sw2\Croncom\CronRunner;
use Sw2\Croncom\TimeStorage\FileTimeStorage;

/**
 * Class CroncomExtension
 *
 * @package Sw2\Croncom\DI
 */
class CroncomExtension extends CompilerExtension
{
	public $defaults = [
		'tasks' => [],
		'locksDir' => '%tempDir%/locks',
		'timeStorage' => FileTimeStorage::class,
	];

	/**
	 * @inheritdoc
	 */
	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();
		$config = $this->getConfig($this->defaults);
		$runner = $builder->addDefinition($this->prefix('runner'))
			->setClass(CronRunner::class, [$this->prefix('@timeStorage')])
			->addTag(ConsoleExtension::TAG_COMMAND)
			->setAutowired(FALSE)->setInject(FALSE);
		$builder->addDefinition($this->prefix('timeStorage'))
			->setClass($config['timeStorage'])
			->setAutowired(TRUE)->setInject(FALSE);

		foreach ($config['tasks'] as $i => $class) {
			$builder->addDefinition($this->prefix("task.$i"))
				->setClass($class, [$config['locksDir']])
				->addSetup('setTimeStorage', [$this->prefix('@timeStorage')])
				->addTag(ConsoleExtension::TAG_COMMAND)
				->setAutowired(FALSE)->setInject(TRUE);

			$runner->addSetup('addTask', [$this->prefix("@task.$i")]);
		}
	}

}
