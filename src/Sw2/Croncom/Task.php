<?php

namespace Sw2\Croncom;

use Nette\Utils\DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Tasks
 *
 * @package Sw2\Croncom
 */
abstract class Task extends Command
{
	/** @var string */
	private $locksDir;

	/** @var ITimeStorage */
	private $timeStorage;

	/**
	 * @param string $locksDir
	 */
	public final function __construct($locksDir)
	{
		parent::__construct();
		$this->locksDir = $locksDir;
	}

	/**
	 * @param ITimeStorage $timeStorage
	 */
	public final function setTimeStorage(ITimeStorage $timeStorage)
	{
		$this->timeStorage = $timeStorage;
	}

	/**
	 * @inheritdoc
	 */
	public final function run(InputInterface $input, OutputInterface $output, DateTime $execTime = NULL)
	{
		$statusCode = 0;
		if ($this->lock()) {
			$output->writeln(sprintf('<info>Task %s</info>', $this->getName()));
			if (($statusCode = parent::run($input, $output)) === 0) {
				$this->timeStorage->putLastTime($this->getName(), $execTime ?: DateTime::from('- 4 seconds'));
			}
		}

		return $statusCode;
	}

	/**
	 * @return bool
	 */
	private function lock()
	{
		static $lock; // static for lock until the process end
		@mkdir($this->locksDir);
		$path = sprintf('%s/croncom-%s.lock', $this->locksDir, md5($this->getName()));
		$lock = fopen($path, 'w+b');

		return $lock !== FALSE && flock($lock, LOCK_EX | LOCK_NB);
	}

}