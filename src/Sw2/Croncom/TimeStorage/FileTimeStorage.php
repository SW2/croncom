<?php

namespace Sw2\Croncom\TimeStorage;

use Nette\DI\Container;
use Nette\Utils\DateTime;
use Sw2\Croncom\ITimeStorage;

/**
 * Class FileTimeStorage
 *
 * @package Sw2\Croncom\TimeStorage
 */
class FileTimeStorage implements ITimeStorage
{
	const TIME_FORMAT = 'Y-m-d H:i:s O';

	/** @var string */
	private $timesDir;

	/**
	 * @param Container $container
	 */
	public function __construct(Container $container)
	{
		$this->timesDir = $container->parameters['tempDir'] . '/cron-times';
		@mkdir($this->timesDir);
	}

	/**
	 * @param string $taskName
	 *
	 * @return DateTime
	 */
	public function getLastTime($taskName)
	{
		$file = $this->getFile($taskName);
		if (file_exists($file)) {
			$content = explode("\r\n", file_get_contents($file));
			if (isset($content[1])) {
				return DateTime::createFromFormat(self::TIME_FORMAT, $content[1]);
			}
		}

		return NULL;
	}

	/**
	 * @param string $taskName
	 * @param DateTime $time
	 */
	public function putLastTime($taskName, DateTime $time)
	{
		file_put_contents($this->getFile($taskName), $taskName . "\r\n" . $time->format(self::TIME_FORMAT));
	}

	/**
	 * @param string $name
	 *
	 * @return string
	 */
	private function getFile($name)
	{
		return 'safe://' . $this->timesDir . '/' . md5($name);
	}

}
